<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package salesrox
 */
global $configuracao;
?>

<!-- RODAPÉ -->
<footer class="rodape">
	<div class="containerFull">
		<div class="tituloFormulario">
			<h1><strong><?php echo $configuracao['opt_rodape_titulo']; ?></strong></h1>
			<p><?php echo $configuracao['opt_rodape_subtitulo']; ?></p>
		</div>
	</div>
	<div class="formularioContato">
		<div class="containerFull">
			<script type="text/javascript">
				/* <![CDATA[ */
				var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://salesrox.com/br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
				/* ]]> */
			</script><script type="text/javascript" src="http://salesrox.com/br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
			<!--END Scripts-->
			<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html5be314ac2de07-4" class="wysija-msg ajax"></div><form id="form-wysija-html5be314ac2de07-4" method="post" action="#wysija" class="widget_wysija html_wysija">
				<input type="text" name="wysija[user][firstname]" class="wysija-input validate[required]" title="Nome" placeholder="Nome" value="" />
				<span class="abs-req">
					<input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
				</span>
				<input type="text" name="wysija[field][cf_1]" class="wysija-input validate[required,custom[phone]]" title="Telefone" placeholder="Telefone" value="" />
				<span class="abs-req">
					<input type="text" name="wysija[field][abs][cf_1]" class="wysija-input validated[abs][cf_1]" value="" />
				</span>
				<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email" placeholder="Email" value="" />
				<span class="abs-req">
					<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
				</span>
				<div class="btnEnviarinput"><input class="wysija-submit wysija-submit-field" type="submit" value="Agende agora uma conversa " /></div>
				<input type="hidden" name="form_id" value="4" />
				<input type="hidden" name="action" value="save" />
				<input type="hidden" name="controller" value="subscribers" />
				<input type="hidden" value="1" name="wysija-page" />
				<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
			</form></div>
		</div>
	</div>
	<div class="copyright">
		<p><?php echo $configuracao['opt_rodape_copyright']; ?></p>
	</div>
</footer>
<?php wp_footer(); ?>

</body>
</html>
