$(function(){
	if($('.pg-inicial .comoFunciona .textoComoFunciona p.fora')){
		//nao faz nada
		var abcde = true;
	}
	else{
		$('.pg-inicial .comoFunciona .textoComoFunciona p').addClass('fora');
	}

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});
		
	$(document).scroll(function () {
		var alturaScroll =  $(document).scrollTop();
		if(alturaScroll > 150){
			$('header').addClass('thin');
		}else{
			$('header').removeClass('thin');
		}
	});
	var largura = $(document).width();
	if(largura > 768){ 
		$('.scrollTop').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - 100
					}, 1000);
					return false;
				}
			}
		});
		
	}
	else{
		$('.scrollTop').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - 25
					}, 1000);
					return false;
				}
			}

		});
	}

	$.fn.isOnScreen = function(){
		var win = $(window);
		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};

		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();

		var bounds = this.offset();
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();

		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
	};

	var largura = $(document).width();
	if(largura > 768){ 
		$(window).scroll(function(){
			if($('.pg-inicial .comoFunciona').isOnScreen()){
				$('.pg-inicial .comoFunciona .bordaVantagens').addClass('show');
				$('.pg-inicial .comoFunciona .textoComoFunciona p').removeClass('fora');
				$('.pg-inicial .comoFunciona .tecnologias').removeClass('fora');
				$('.pg-inicial .comoFunciona .vantagesSalesrox .vantagens').removeClass('fora');
			}
		});
	}
	else{
		$('.pg-inicial .comoFunciona .bordaVantagens').addClass('show');
		$('.pg-inicial .comoFunciona .textoComoFunciona p').removeClass('fora');
		$('.pg-inicial .comoFunciona .vantagesSalesrox .vantagens').removeClass('fora');
		$('.pg-inicial .comoFunciona .tecnologias').removeClass('fora');
	}
});