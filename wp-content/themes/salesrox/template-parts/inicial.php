<?php
/**
 * Template name: Inicial
 * Template part for initial page
 *
 * 
 *
 * @package salesrox
 */
get_header();
?>

<!-- PG INICIAL -->
<div class="pg pg-inicial">
	<section class="inicial">
		<div class="containerFull">
			<h6 class="hidden">Sessão Inicial</h6>
			<div class="textoInicial">
				<h2><?php echo $configuracao['opt_titulo']; ?></h2>
				<p><?php echo $configuracao['opt_descricao']; ?></p>
			</div>
			<div class="formularioContato">
				<!--START Scripts : this is the script part you can add to the header of your theme-->
				<script type="text/javascript" src="http://salesrox.com/br/wp-includes/js/jquery/jquery.js?ver=2.10.2"></script>
				<script type="text/javascript" src="http://salesrox.com/br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.10.2"></script>
				<script type="text/javascript" src="http://salesrox.com/br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.10.2"></script>
				<script type="text/javascript" src="http://salesrox.com/br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
				<script type="text/javascript">
					/* <![CDATA[ */
					var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://salesrox.com/br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
					/* ]]> */
				</script><script type="text/javascript" src="http://salesrox.com/br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
				<!--END Scripts-->

				<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html5be31056b9289-3" class="wysija-msg ajax"></div><form id="form-wysija-html5be31056b9289-3" method="post" action="#wysija" class="widget_wysija html_wysija">
					<input type="text" name="wysija[user][firstname]" class="wysija-input validate[required]" title="Nome" placeholder="Nome" value="" />
					<span class="abs-req">
						<input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
					</span>
					<input type="text" name="wysija[field][cf_1]" class="wysija-input validate[custom[phone]]" title="Telefone" placeholder="Telefone" value="" />
					<span class="abs-req">
						<input type="text" name="wysija[field][abs][cf_1]" class="wysija-input validated[abs][cf_1]" value="" />
					</span>
					<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email" placeholder="Email" value="" />
					<span class="abs-req">
						<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
					</span>
					<div class="btnEnviarinput"><input class="wysija-submit wysija-submit-field" type="submit" value="Agende agora uma conversa" /></div>
					<input type="hidden" name="form_id" value="3" />
					<input type="hidden" name="action" value="save" />
					<input type="hidden" name="controller" value="subscribers" />
					<input type="hidden" value="1" name="wysija-page" />
					<input type="hidden" name="wysija[user_list][list_ids]" value="1" />

				</form></div>
			</div>
		</div>
	</section>
	<div class="scrollDown">
		<a href="#comoFunciona" class="scrollTop"></a>
	</div>

	<section class="comoFunciona" id="comoFunciona">
		<div class="containerFull overX">
			<div class="row">
				<div class="col-md-6">
					<div class="textoComoFunciona">
						<h3><?php echo $configuracao['opt_comoFunciona_titulo']; ?></h3>
						<?php echo $configuracao['opt_comoFunciona_conteudo']; ?>
					</div>
					<div class="tecnologias fora">
						<ul>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/img/trsaudio.png" alt="Transcrição de Áudio">
								<p>Transcrição de Áudio</p>
							</li>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/img/ia.png" alt="Transcrição de Áudio">
								<p>Inteligência Artificial</p>
							</li>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/img/coaching.png" alt="Transcrição de Áudio">
								<p>Coaching Features</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-md-6">
					<div class="bordaVantagens"></div>
					<div class="vantagesSalesrox">
						<h2><?php echo $configuracao['opt_vantagens_titulo']; ?></h2>
						<div class="vantagens fora">
							<ul>
								<li>
									<article>
										<figure class="imagemDestaque">
											<img src="<?php echo $configuracao['opt_vantagens_topico_1_img']['url']; ?>" alt="Conversão">
										</figure>
										<h1><?php echo $configuracao['opt_vantagens_topico_1_titulo']; ?></h1>
									</article>
								</li>
								<li>
									<article>
										<figure class="imagemDestaque">
											<img src="<?php echo $configuracao['opt_vantagens_topico_2_img']['url']; ?>" alt="Conversão">
										</figure>
										<h1><?php echo $configuracao['opt_vantagens_topico_2_titulo']; ?></h1>
									</article>
								</li>
								<li>
									<article>
										<figure class="imagemDestaque">
											<img src="<?php echo $configuracao['opt_vantagens_topico_3_img']['url']; ?>" alt="Conversão">
										</figure>
										<h1><?php echo $configuracao['opt_vantagens_topico_3_titulo']; ?></h1>
									</article>
								</li>
								<li>
									<article>
										<figure class="imagemDestaque">
											<img src="<?php echo $configuracao['opt_vantagens_topico_4_img']['url']; ?>" alt="Conversão">
										</figure>
										<h1><?php echo $configuracao['opt_vantagens_topico_4_titulo']; ?></h1>
									</article>
								</li>
							</ul>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="algumasLogos">
		<div class="containerFull">
			<ul class="listadeLogos">
				<li><a href="<?php echo $configuracao['opt_logos1_link'] ?>"><img src="<?php echo $configuracao['opt_logos1']['url'] ?>" alt=""></a></li>
				<li><a href="<?php echo $configuracao['opt_logos2_link'] ?>"><img src="<?php echo $configuracao['opt_logos2']['url'] ?>" alt=""></a></li>
				<li><a href="<?php echo $configuracao['opt_logos3_link'] ?>"><img src="<?php echo $configuracao['opt_logos3']['url'] ?>" alt=""></a></li>
				<li><a href="<?php echo $configuracao['opt_logos4_link'] ?>"><img src="<?php echo $configuracao['opt_logos4']['url'] ?>" alt=""></a></li>
				<li><a href="<?php echo $configuracao['opt_logos5_link'] ?>"><img src="<?php echo $configuracao['opt_logos5']['url'] ?>" alt=""></a></li>
			</ul>
		</div>
	</div>
</div>

<?php get_footer(); ?>