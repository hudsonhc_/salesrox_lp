<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package salesrox
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" /> 
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- TOPO -->
	<header class="topo">
		<div class="row">
			<!-- LOGO -->
			<div class="col-xs-6">
				<a href="<?php echo get_home_url() ?>">
					<img class="img-responsive" src="<?php echo $configuracao['opt_logo']['url']; ?>" alt="Salesrox">
				</a>
			</div>
			<!-- MENU  -->	
			<div class="col-xs-6">
				<div class="language">
					<ul>
						<li class="selected">
							<span>BR <img src="<?php echo get_template_directory_uri() ?>/img/brazil.svg" alt="Portugues"></span>
							<ul>
								<li>
									<a href="http://salesrox.com">
										<span>EN <img src="<?php echo get_template_directory_uri() ?>/img/usa.svg" alt="English"></span>
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>
